FROM dpokidov/imagemagick

WORKDIR /opt

RUN yum install -y ghostscript && yum clean all

ENTRYPOINT ["convert"]
