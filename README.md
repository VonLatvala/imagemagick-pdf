## Usage

By default container will run convert command

```
$ docker run -v /your/images:/imgs vonlatvala/imagemagick-pdf /imgs/sample.png -resize 100x100 /imgs/resized-sample.png
```
You can change entrypoint and pass other IM commands to execute. For instance,

```
$ docker run --entrypoint=identify -v /your/images:/imgs vonlatvala/imagemagick-pdf /imgs/sample.png
```
